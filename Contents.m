% Neurophysiological Biomarker Toolbox (NBT) 
% Version NBT v.0.5.3-alpha

% NBT Toolbox --
% Use the NBTwiki at http://www.nbtwiki.net

% Copyright (C) 2008-2015  The developers
% Main development team: Simon-Shlomo Poil, Sonja Simpraga, Richard Hardstone, Klaus Linkenkaer-Hansen.
% 
